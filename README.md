# Hugo Pipeline

[![pipeline status](https://gitlab.com/brettops/pipelines/hugo/badges/main/pipeline.svg)](https://gitlab.com/brettops/pipelines/hugo/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

Build static sites with [Hugo](https://gohugo.io/).

[View the example site](https://brettops.gitlab.io/pipelines/hugo/)

## Usage

Add the `hugo` pipeline to the `.gitlab-ci.yml` file:

```yaml
include:
  - project: brettops/pipelines/hugo
    file: include.yml
```

The resulting site is captured as an artifact in the `public/` directory.

Add the [pages](https://gitlab.com/brettops/pipelines/pages) pipeline to
publish the site to
[GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/):

```yaml
include:
  - project: brettops/pipelines/hugo
    file: include.yml
  - project: brettops/pipelines/pages
    file: include.yml
```

An [example site](https://brettops.gitlab.io/pipelines/hugo/) is published by this pipeline.
